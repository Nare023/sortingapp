package org.example;


import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        if (args != null){
            if (args.length <= 10) {
                Arrays.sort(args);
                System.out.print(Arrays.toString(args));
            } else {
                System.out.print("Error: Too many arguments.");
            }
        } else {
            throw new NullPointerException();
        }
    }
}
