package org.example;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

import static junit.framework.TestCase.assertEquals;

/**
 * Unit test for the App class.
 */

@RunWith(Parameterized.class)
public class AppTest {
    private String[] inputArgs;
    private String expectedOutput;

    /**
     * Constructor for AppTest.
     *
     * @param inputArgs      The input arguments to be passed to the main method of the class.
     * @param expectedOutput The expected output after running the main method.
     */
    public AppTest(String[] inputArgs, String expectedOutput) {
        this.inputArgs = inputArgs;
        this.expectedOutput = expectedOutput;
    }

    /**
     *
     * @return A collection of input arguments and expected outputs.
     */
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                // Test case with zero arguments
                {new String[]{}, "[]"},

                // Test case with one argument
                {new String[]{"5"}, "[5]"},

                // Test case with ten arguments
                {new String[]{"5", "4", "2", "9", "1", "3", "7", "6", "8", "0"}, "[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]"},

                // Test case with more than ten arguments
                {new String[]{"5", "4", "2", "9", "1", "3", "7", "6", "8", "0", "11"}, "Error: Too many arguments."},
        });
    }
    @Test
    public void testMain() {
        // Redirect standard output to capture printed messages
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        App.main(inputArgs);

        assertEquals(expectedOutput, outContent.toString());
    }


}
